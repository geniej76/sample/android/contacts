package com.momolab.contact.view

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.momolab.contact.data.Contact
import com.momolab.contact.data.ContactsDataSourceFactory
import java.util.concurrent.Executors

class ContactsViewModel(application: Application) : AndroidViewModel(application) {
    companion object {
        private const val TAG = "ContactsViewModel"

        private const val INITIAL_LOAD_SIZE_HINT = 20
        private const val PAGE_SIZE = 100
        private const val THREAD_COUNT = 1
        private const val PREFETCH_DISTANCE = 100
    }

    private var items: LiveData<PagedList<Contact>>

    private val query by lazy {
        MutableLiveData<String>()
    }

    init {
        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setInitialLoadSizeHint(INITIAL_LOAD_SIZE_HINT)
            .setPageSize(PAGE_SIZE) //                .setPrefetchDistance(PREFETCH_DISTANCE)
            .build()

        items = Transformations.switchMap(query) { query ->
            when (query) {
                null, "" -> {
                    var livePagedListBuilder = LivePagedListBuilder(
                        ContactsDataSourceFactory(
                            application.applicationContext,
                            ""
                        ),
                        pagedListConfig
                    ).setFetchExecutor(Executors.newFixedThreadPool(THREAD_COUNT))

                    livePagedListBuilder.build()
                }
                else -> {
                    val contactDataSourceFactory =
                        ContactsDataSourceFactory(
                            application.applicationContext,
                            query
                        )

                    // get events that match the title
                    var livePagedListBuilder = LivePagedListBuilder(
                        contactDataSourceFactory,
                        pagedListConfig
                    ).setFetchExecutor(Executors.newFixedThreadPool(THREAD_COUNT))

                    livePagedListBuilder.build()
                }
            }
        }
    }

    fun items(): LiveData<PagedList<Contact>> {
        return items
    }

    fun setQuery(queryStr: String) {
        query.value = queryStr.trim()
    }
}
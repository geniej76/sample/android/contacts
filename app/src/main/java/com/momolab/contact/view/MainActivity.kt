package com.momolab.contact.view

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.paging.PagedList
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.android.material.snackbar.Snackbar
import com.momolab.contact.R
import com.momolab.contact.data.Contact
import com.momolab.contact.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var contactViewModel: ContactsViewModel

    companion object {
        const val TAG = "MainActivity"

        const val PERMISSIONS_REQUEST_CODE = 0

        var REQUIRED_PERMISSIONS = arrayOf(
            Manifest.permission.READ_CONTACTS
        )
    }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this,
            R.layout.activity_main
        )

        binding.srlContact.setOnRefreshListener {
            binding.srlContact.isRefreshing = false;
        }

        val dividerItemDecoration =
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL)

        ContextCompat.getDrawable(
            this,
            R.drawable.shape_devide_tb_line
        )?.let {
            dividerItemDecoration.setDrawable(
                it
            )
        }
        binding.rvContact.addItemDecoration(dividerItemDecoration)

        binding.etSearch.setOnEditorActionListener { v, actionId, event ->
            when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    Log.d(TAG, "$TAG search")

                    getData(binding.etSearch.text.toString())

                    true
                }
                else -> false
            }
        }

        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                Log.d(TAG, "$TAG beforeTextChanged $s")
            }

            override fun afterTextChanged(s: Editable) {
                Log.d(TAG, "$TAG afterTextChanged $s")
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                Log.d(TAG, "$TAG onTextChanged $s")

                getData(s.toString())
            }
        })

        contactViewModel = ViewModelProvider(this).get(ContactsViewModel::class.java)

        requestContactPermission()
    }

    private fun requestContactPermission() {
        var contactPermission =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)

        if (contactPermission === PackageManager.PERMISSION_GRANTED) {
            initView()
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    REQUIRED_PERMISSIONS[0]
                )
            ) {
                Snackbar.make(
                    binding.vgMain, "이 앱을 실행하려면 주소록 읽기 권한이 필요합니다.",
                    Snackbar.LENGTH_INDEFINITE
                ).setAction("확인", PermissionConfirmListener()).show()
            } else {
                // 4-1. 사용자가 퍼미션 거부를 한 적이 없는 경우에는 퍼미션 요청을 바로 합니다.
                // 요청 결과는 onRequestPermissionResult에서 수신됩니다.
                ActivityCompat.requestPermissions(
                    this,
                    REQUIRED_PERMISSIONS,
                    PERMISSIONS_REQUEST_CODE
                )
            }
        }
    }

    inner class PermissionConfirmListener : View.OnClickListener {
        override fun onClick(v: View) {
            ActivityCompat.requestPermissions(
                this@MainActivity,
                REQUIRED_PERMISSIONS,
                PERMISSIONS_REQUEST_CODE
            )
        }
    }

    private fun initView() {
        var contactAdapter = ContactsAdapter()
        binding.rvContact.adapter = contactAdapter

        contactViewModel.items().observe(
            this,
            object : Observer<PagedList<Contact>> {
                override fun onChanged(items: PagedList<Contact>) {
                    if (isFinishing) {
                        return
                    }
                    contactAdapter.submitList(items)
                }
            })

        getData(binding.etSearch.text.toString())
    }

    private fun getData(query: String) {
        contactViewModel?.run {
            setQuery(query)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSIONS_REQUEST_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED)
                ) {
                    initView()
                } else {
                    finish()
                }

                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }
}
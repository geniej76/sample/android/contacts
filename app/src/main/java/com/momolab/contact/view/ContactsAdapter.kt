package com.momolab.contact.view

import android.telephony.PhoneNumberUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.momolab.contact.data.Contact
import com.momolab.contact.databinding.ItemContactBinding
import java.util.*

class ContactsAdapter :
    PagedListAdapter<Contact, ContactsAdapter.ContactsViewHolder>(
        DIFF_CALLBACK
    ) {

    companion object {
        private val DIFF_CALLBACK = object :
            DiffUtil.ItemCallback<Contact>() {
            // Concert details may have changed if reloaded from the database,
            // but ID is fixed.
            override fun areItemsTheSame(
                oldItem: Contact,
                newItem: Contact
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: Contact,
                newItem: Contact
            ): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ContactsViewHolder {
        return ContactsViewHolder(
            ItemContactBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(viewHolder: ContactsViewHolder, position: Int) {
        val item: Contact? = getItem(position)
        if (item != null) {
            viewHolder.bindTo(item)
        } else {
            // Null defines a placeholder item - PagedListAdapter will automatically invalidate
            // this row when the actual object is loaded from the database
            viewHolder.clear()
        }
    }

    inner class ContactsViewHolder(binding: ItemContactBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private val binding: ItemContactBinding = binding

        fun bindTo(item: Contact) {
            binding.tvId.text = item.id.toString()
            binding.tvMemberName.text = item.name
            binding.tvMemberNumber.text = PhoneNumberUtils.formatNumber(
                item.phoneNo,
                Locale.getDefault().country
            )
        }

        fun clear() {}
    }
}
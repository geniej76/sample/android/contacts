package com.momolab.contact.data

data class Contact(
    val seq: Int,
    val id: Long,
    val photoId: String?,
    val name: String,
    val phoneNo: String,
    val email: String,
    val sectionName: String
)
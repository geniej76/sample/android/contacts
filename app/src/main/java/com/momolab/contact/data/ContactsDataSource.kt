package com.momolab.contact.data

import android.content.Context
import androidx.paging.ItemKeyedDataSource

class ContactsDataSource(private val context: Context, private val query: String) :
    ItemKeyedDataSource<Int, Contact>() {
    
    companion object {
        const val TAG = "ContactsDataSource"
    }

    override fun getKey(item: Contact): Int {
        return item.seq
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Contact>
    ) {
        Contacts(context).getInitContacts(query, params.requestedLoadSize)?.run {
            callback.onResult(this)
        }
    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Contact>
    ) {
        Contacts(context)
            .getBeforeContacts(query, params.key, params.requestedLoadSize)?.run {
            callback.onResult(this)
        }
    }

    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Contact>
    ) {
        Contacts(context)
            .getAfterContacts(query, params.key, params.requestedLoadSize)?.run {
            callback.onResult(this)
        }
    }
}
package com.momolab.contact.data

import android.content.Context
import android.net.Uri
import android.provider.ContactsContract
import java.util.*

class Contacts(private val context: Context) {
    companion object {
        const val TAG = "Contacts"
        val URI: Uri = ContactsContract.Contacts.CONTENT_URI
        val INITIAL_HEADER = arrayOf(
            "ㄱ",
            "ㄲ",
            "ㄴ",
            "ㄷ",
            "ㄸ",
            "ㄹ",
            "ㅁ",
            "ㅂ",
            "ㅃ",
            "ㅅ",
            "ㅆ",
            "ㅇ",
            "ㅈ",
            "ㅉ",
            "ㅊ",
            "ㅋ",
            "ㅌ",
            "ㅍ",
            "ㅎ",
            "ABC",
            "123",
            "etc"
        )
    }

    fun getInitContacts(
        query: String,
        limit: Int
    ): List<Contact> {
        return getContacts(
            query = query,
            limit = limit
        )
    }

    fun getAfterContacts(
        query: String,
        offset: Int,
        limit: Int
    ): List<Contact> {
        return getContacts(
            query = query,
            offset = offset + 1,
            limit = limit
        )
    }

    fun getBeforeContacts(
        query: String,
        offset: Int,
        limit: Int
    ): List<Contact> {
//        return getContacts(ContactsContract.Contacts.DISPLAY_NAME + " < '" + offset + "'", null, ContactsContract.Contacts.DISPLAY_NAME +
//                " COLLATE LOCALIZED ASC LIMIT " + limit);
        return ArrayList()
    }

    private fun getContacts(
        query: String,
        offset: Int = 0,
        limit: Int
    ): List<Contact> {
        val contentResolver = context.contentResolver

        val contacts: ArrayList<Contact> = arrayListOf()

        contentResolver.query(
            URI,
            null,
            if (query == "") {
                null
            } else {
                " " + ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY + " like '%$query%' "
            },
            null,
            ContactsContract.Contacts.DISPLAY_NAME_PRIMARY +
                    " COLLATE LOCALIZED ASC LIMIT " + limit + " OFFSET " + offset
        )?.run {
            var increase = 0

            while (moveToNext()) {
                val id = getLong(getColumnIndex(ContactsContract.Contacts._ID))
                val photoId = getString(getColumnIndex(ContactsContract.Contacts.PHOTO_ID))
                val name =
                    getString(getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY))

                var phoneNo = ""
                var email = ""

                contentResolver.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                    null,
                    null
                )?.run {
                    if (moveToFirst()) {
                        phoneNo =
                            getString(getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    }
                    close()
                }

                contentResolver.query(
                    ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                    arrayOf(id.toString()),
                    null
                )?.run {
                    if (moveToFirst()) {
                        email =
                            getString(getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))
                    }
                    close()
                }

                if (phoneNo.startsWith("01") || phoneNo.startsWith("+")) {
                    contacts.add(
                        Contact(
                            seq = offset + increase,
                            id = id,
                            photoId = photoId,
                            name = name,
                            phoneNo = phoneNo,
                            email = email,
                            sectionName = getSectionName(name)
                        )
                    )
                }
                increase += 1
            }
            close()
        }

        return contacts
    }

    private fun getSectionName(name: String): String {
        if (name == "") {
            return INITIAL_HEADER[21]
        }

        val initialNumberSound: String = getInitialNumber(name)
        val initialEngSound: String = getInitialEngSound(name)
        val initialKoreanSound: String = getInitialKoreanSound(name)
        if (initialNumberSound != "") {
            return INITIAL_HEADER[20]
        }
        if (initialEngSound != "") {
            return INITIAL_HEADER[19]
        }
        if (initialKoreanSound == "") {
            return INITIAL_HEADER[21]
        }
        val intKoreanIndex: Int = getInitialKoreanIndex(initialKoreanSound)

        return if (intKoreanIndex > -1) {
            INITIAL_HEADER[intKoreanIndex]
        } else {
            INITIAL_HEADER[21]
        }
    }

    private fun getInitialKoreanIndex(initialKoreanSound: String): Int {
        for (i in 0..18) {
            if (INITIAL_HEADER[i] == initialKoreanSound) {
                return i
            }
        }
        return -1
    }

    private fun getInitialNumber(text: String): String {
        if (text.isEmpty()) {
            return ""
        }
        val chName = text[0]
        return if (chName.toInt() in 0x30..0x39) {
            chName.toString()
        } else ""
    }

    private fun getInitialEngSound(text: String): String {
        if (text.isEmpty()) {
            return ""
        }
        val chName = text[0]
        return if (chName.toInt() in 0x41..0x5a || chName.toInt() in 0x61..0x7a) {
            chName.toString()
        } else ""
    }

    private fun getInitialKoreanSound(text: String): String {
        val chs = arrayOf(
            "ㄱ", "ㄲ", "ㄴ", "ㄷ", "ㄸ",
            "ㄹ", "ㅁ", "ㅂ", "ㅃ", "ㅅ",
            "ㅆ", "ㅇ", "ㅈ", "ㅉ", "ㅊ",
            "ㅋ", "ㅌ", "ㅍ", "ㅎ"
        )

        if (text.isEmpty()) {
            return ""
        }

        val chName = text[0]
        if (chName.toInt() >= 0xAC00) {
            val uniVal = chName.toInt() - 0xAC00
            val cho = (uniVal - uniVal % 28) / 28 / 21
            return if (cho >= chs.size) {
                ""
            } else {
                chs[cho]
            }
        }

        return ""
    }
}
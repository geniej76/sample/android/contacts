package com.momolab.contact.data

import android.content.Context
import androidx.paging.DataSource

class ContactsDataSourceFactory(private val context: Context, private val query: String) :
    DataSource.Factory<Int, Contact>() {

    override fun create(): DataSource<Int, Contact> {
        return ContactsDataSource(context, query)
    }
}